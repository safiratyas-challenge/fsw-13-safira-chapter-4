class Car {
  static list = [];

  static init(cars) {
    this.list = cars.map((i) => new this(i));
  }

  constructor({
    id,
    plate,
    manufacture,
    model,
    image,
    rentPerDay,
    capacity,
    description,
    transmission,
    available,
    type,
    year,
    options,
    specs,
    availableAt,
  }) {
    this.id = id;
    this.plate = plate;
    this.manufacture = manufacture;
    this.model = model;
    this.image = image;
    this.rentPerDay = rentPerDay;
    this.capacity = capacity;
    this.description = description;
    this.transmission = transmission;
    this.available = available;
    this.type = type;
    this.year = year;
    this.options = options;
    this.specs = specs;
    this.availableAt = availableAt;
  }

  render() {
    return `
    <div class="container">
      <div class="row">
        <div class="col-sm-4 d-flex align-items-stretch" style="margin-top: 5%;">
          <div class="card" style="-webkit-box-shadow: 0 0 10px #F1F3FF; box-shadow: 0 0 10px #F1F3FF; border-radius: 8px;">
            <div class="card-body">
              <img src="${this.image}" style="width: 100%; object-fit: cover;">
              <h5 class="card-title mt-4" style="font-weight: 400; font-size: 16px">${this.manufacture}/${this.model}
              </h5>
              <p style="font-weight:bold;">Rp.${this.rentPerDay}/hari</p>
              <p class="card-text" style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">
                ${this.description}</p>
              <li class="list-fi mb-3">
                <img src="images/fi_users.png" style="width: 7%;">
                ${this.capacity} Orang</li>
              <li class="list-fi mb-3">
                <img src="images/fi_settings.png" style="width: 7%;">
                ${this.transmission}</li>
              <li class="list-fi mb-3">
                <img src="images/fi_calendar.png" style="width: 7%;">
                Tahun ${this.year}</li>
              <a href="#" class="btn btn-success" style="width: 100%; margin-top: 5%;">Pilih Mobil</a>
            </div>
          </div>
        </div>
        <div class="col-sm-4 d-flex align-items-stretch" style="margin-top: 5%;">
          <div class="card" style="-webkit-box-shadow: 0 0 10px #F1F3FF; box-shadow: 0 0 10px #F1F3FF; border-radius: 8px;">
            <div class="card-body">
              <img src="${this.image}" style="width: 100%; object-fit: cover;">
              <h5 class="card-title mt-4" style="font-weight: 400; font-size: 16px">${this.manufacture}/${this.model}
              </h5>
              <p style="font-weight:bold;">Rp.${this.rentPerDay}/hari</p>
              <p class="card-text" style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">
                ${this.description}</p>
              <li class="list-fi mb-3">
                <img src="images/fi_users.png" style="width: 7%;">
                ${this.capacity} Orang</li>
              <li class="list-fi mb-3">
                <img src="images/fi_settings.png" style="width: 7%;">
                ${this.transmission}</li>
              <li class="list-fi mb-3">
                <img src="images/fi_calendar.png" style="width: 7%;">
                Tahun ${this.year}</li>
              <a href="#" class="btn btn-success" style="width: 100%; margin-top: 5%;">Pilih Mobil</a>
            </div>
          </div>
        </div>
        <div class="col-sm-4 d-flex align-items-stretch" style="margin-top: 5%;">
          <div class="card" style="-webkit-box-shadow: 0 0 10px #F1F3FF; box-shadow: 0 0 10px #F1F3FF; border-radius: 8px;">
            <div class="card-body">
              <img src="${this.image}" style="width: 100%; object-fit: cover;">
              <h5 class="card-title mt-4" style="font-weight: 400; font-size: 16px">${this.manufacture}/${this.model}
              </h5>
              <p style="font-weight:bold;">Rp.${this.rentPerDay}/hari</p>
              <p class="card-text" style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">
                ${this.description}</p>
              <li class="list-fi mb-3">
                <img src="images/fi_users.png" style="width: 7%;">
                ${this.capacity} Orang</li>
              <li class="list-fi mb-3">
                <img src="images/fi_settings.png" style="width: 7%;">
                ${this.transmission}</li>
              <li class="list-fi mb-3">
                <img src="images/fi_calendar.png" style="width: 7%;">
                Tahun ${this.year}</li>
              <a href="#" class="btn btn-success" style="width: 100%; margin-top: 5%;">Pilih Mobil</a>
            </div>
          </div>
        </div>
        `;
        }
      }